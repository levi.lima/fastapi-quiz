from typing import List

from pydantic import BaseModel

class OptionBase(BaseModel):
    option: str
    correct: bool

class CreateOption(OptionBase):
    pass

class Option(OptionBase):
    id: int
    question_id: int
    
    class Config:
        orm_mode=True


class QuestionBase(BaseModel):
    question: str
    
class CreateQuestion(QuestionBase):
    pass

class Question(QuestionBase):
    id: int
    quiz_id: int
    options: List[Option] = []
    
    class Config:
        orm_mode=True

        
class QuizBase(BaseModel):
    name: str

class CreateQuiz(QuizBase):
    pass

class Quiz(QuizBase):
    id: int
    question_relation: List[Question] = []
    
    class Config:
        orm_mode=True

class Check(BaseModel):
    id: int
    