from typing import List, Optional

from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import engine, SessionLocal

models.Base.metadata.create_all(bind = engine)

app = FastAPI(debug = True)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post('/quiz/create/', response_model = schemas.Quiz)
async def create_quiz(quiz: schemas.CreateQuiz, db: Session = Depends(get_db)):
    db_quiz = crud.get_to_name(models.Quiz, db, name = quiz.name)
    if db_quiz:
        raise HTTPException(
            status_code = 400,
            detail = "Quiz already exists"
        )
    return crud.create_quiz(db = db, quiz = quiz)

@app.post('/quiz/create/{quiz_id}/quest/', response_model = schemas.Question)
async def create_question(quiz_id: int, quest: schemas.CreateQuestion, db: Session = Depends(get_db)):
    db_quest = crud.get_to_id(models.Quiz, db, id = quiz_id)
    if db_quest:
        return crud.create_question(
            db = db,
            quest = quest,
            quiz_id = quiz_id
        )
    raise HTTPException(
        status_code = 404,
        details = 'Not found the quiz'
    )
    
@app.post('/quiz/create/{quest_id}/option/', response_model = schemas.Option)
async def create_option(quest_id: int, option: schemas.CreateOption, db: Session = Depends(get_db)):
    db_option = crud.get_to_id(models.Question, db, id = quest_id)
    if db_option: 
        return crud.create_option(
            db = db,
            option = option,
            quest_id = quest_id
        )
    raise HTTPException(
        status_code = 404,
        details = 'Not found the quest'
    )

@app.get('/quiz/select/', response_model = List[schemas.Quiz])
async def read_all_quizes(db: Session = Depends(get_db), skip: int = 0, limit: int = 100):
    db_quiz = crud.get_all(db, skip=skip, limit=limit)
    if db_quiz:
        return db_quiz
    raise HTTPException(
        status_code = 400,
        detail = 'Don`t exist any quiz'
    )

@app.get('/quiz/select/{quiz_id}/', response_model = schemas.Quiz)
async def read_select_quizes(db: Session = Depends(get_db), quiz_id = int):
    db_quiz = crud.get_to_id(models.Quiz, db, id=quiz_id)
    if db_quiz:
        return db_quiz
    raise HTTPException(
        status_code = 400,
        detail = 'This quiz don`t exists'
    )
    
@app.put('/quiz/update/{quiz_id}/', response_model = schemas.Quiz)
async def update_quiz(quiz_id: int, quiz: schemas.CreateQuiz, db: Session = Depends(get_db)):
    db_quiz = crud.get_to_id(models.Quiz, db, id=quiz_id)
    if db_quiz:
        #return db.query(models.Quiz).filter(models.Quiz.id == quiz_id).update({"name": quiz.name}, synchronize_session = 'evaluate')
        return crud.update_quiz(db= db, name= quiz.name, quiz_id= quiz_id)
    raise HTTPException(
        status_code = 400,
        detail = 'This quiz don`t exists'
    )

@app.put('/quiz/update/{quiz_id}/quest/', response_model = schemas.Question)
async def update_question(quiz_id: int, quest: schemas.CreateQuestion, db: Session = Depends(get_db)):
    db_quest = crud.get_to_id(models.Question, db, id=quiz_id)
    if db_quest:
        return crud.update_question(db= db, quest= quest.question, quiz_id=quiz_id)
    raise HTTPException(
        status_code = 400,
        detail = 'This quiz don`t exists'
    )
    
@app.put('/quiz/update/{quiz_id}/option/', response_model = schemas.Option)
async def update_option(quiz_id: int, quiz: schemas.CreateOption, db: Session = Depends(get_db)):
    db_option = crud.get_to_id(models.Option, db, id=quiz_id)
    if db_option:
        return crud.update_option(db=db, data= {k:v for k,v in quiz}, quiz_id= quiz_id)
    raise HTTPException(
        status_code = 400,
        detail = 'This quiz don`t exists'
    )

@app.post('/quiz/play/{quiz_id}/question/{quest_id}/', response_model= schemas.Option)
async def check_response(quiz_id: int, quest_id: int, option_id: schemas.Check, db: Session = Depends(get_db)):
    db_quiz = crud.get_to_id(models.Option, db, id=quiz_id)
    db_quest = crud.get_to_id(models.Option, db, id=quest_id)
    if db_quiz and db_quest:
        return crud.get_to_id(models.Option, db, id= option_id.id)
    raise HTTPException(
        status_code = 400,
        detail = 'This quiz don`t exists'
    )
