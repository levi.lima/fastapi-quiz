from sqlalchemy.orm import Session

from . import models, schemas


def logic_option(*, db: Session, ant: bool, correct: bool, quiz_id: int):
    if correct == True:
        if correct != ant:
            limit_max = quiz_id+1 if int(quiz_id/4)*4 == quiz_id else int(quiz_id/4)*4 +5
            limit_min = limit_max-4
            quiz = db.query(models.Option).filter(models.Option.id == limit_min and models.Option.id == limit_max).\
                update({'correct': False}, synchronize_session= False)
            quiz = db.query(models.Option).get(quiz_id)
            quiz.correct = True

#READ

def get_to_id( _query, db: Session, *, id: int):
    return db.query(_query).filter(_query.id == id).first()

def get_to_name( _query, db: Session, *, name: str):
    return db.query(_query).filter(_query.name == name).first()

def get_all(db: Session, *, skip: int= 0, limit: int= 100):
    return db.query(models.Quiz).offset(skip).limit(limit).all()

#CREATE

def return_db(*, db, params):
    db.add(params)
    db.commit()
    db.refresh(params)
    return params
    
def create_quiz(*, db: Session, quiz: schemas.CreateQuiz):
    db_quiz = models.Quiz(name = quiz.name)
    return return_db(db = db, params = db_quiz)

def create_question(*, db: Session, quest: schemas.CreateQuestion, quiz_id: int):
    db_quest = models.Question(
        **quest.dict(),
        quiz_id = quiz_id
    )
    return return_db(db=db, params=db_quest)

def create_option(*, db: Session, option: schemas.CreateOption, quest_id: int):
    db_option = models.Option(
        **option.dict(),
        question_id = quest_id
    )
    return return_db(db=db, params=db_option)

#UPDATE
    
def update_quiz(*, db: Session, name: str, quiz_id: int):
    quiz = db.query(models.Quiz).get(quiz_id)
    quiz.name = name
    db.commit()
    return quiz

def update_question(*, db: Session, quest: str, quiz_id: int):
    quest_db = db.query(models.Question).get(quiz_id)
    quest_db.question = quest
    db.commit()
    return quest_db
        
def update_option(*, db: Session, data: dict, quiz_id: int):
    quiz = db.query(models.Option).get(quiz_id)
    logic_option(db= db, ant= quiz.correct, correct= data.get('correct'), quiz_id= quiz_id)
    quiz.option = data.get('option')
    db.commit()
    return quiz
