from sqlalchemy import Column, String, Text, Integer, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from .database import Base

class Option(Base):
    __tablename__ = "option"
    
    id = Column(Integer, primary_key=True, index=True)
    option = Column(Text)
    correct = Column(Boolean, default = False)
    question_id = Column(Integer, ForeignKey('questions.id'))
    
    questions = relationship('Question', back_populates='options')
    
class Question(Base):
    __tablename__ = "questions"
    
    id = Column(Integer, primary_key= True, index=True)
    question = Column(Text)
    quiz_id = Column(Integer, ForeignKey('quizes.id'))
    
    quizes = relationship('Quiz', back_populates='question_relation')
    options = relationship('Option', back_populates='questions')
    
class Quiz(Base):
    __tablename__ = "quizes"
    
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True)
    
    question_relation = relationship('Question', back_populates='quizes')